# Reimbursement #

This summarizes the steps required to get reimbursed for an expense. 

### Step 1: Find the UCalgary page ###

* Log in to MyUCalgary
* Go to My work -> Manage expenses
* Select "Create/Modify expense report"
* Click "Add"

### Step 2: Expense Claim Defaults & Attachments ###

* Business purpose: if unsure, select "General Expense" 
* Report description: briefly describe your expense
* Default location: Select AB from drop down list
* Attachments: Scan all receipts and upload. You must include both Itemized and payment receipt when credit card is used. 
If the payment was not in Canadian Dollars (CAD), include your credit card statement showing the Canadian amount paid.

###  Step 3: Expenses ###
* Date: date on receipt/date of expense
* Expense Type: select from drop down list. Do NOT use PER items these are restricted to Academic staff with PER accounts, (Professional Expense Reimbursement).
 Incidentals is the most likely expense type
* Description: Description of expense, e.g. Nuts & bold for equipment repairs. Be specific.
* Amount: Total including GST. Enter CAD amount based on CC stmt if other than CAD.
* Currency: Leave as CAD

###  Step 4: Accouting details ###
* Account: Auto populated based on Expense Type
* Fund: 60
* Dept: 32010
* Program: (leave blank)
* PC Bus unit: UCP01 
* Project: request from Art
* Activity: 00000
* Leave remaining fields blank

###  Step 5: Add more expenses ###
* Add a new line by clicking + at the end of an expense row.

###  Step 6: Save or Submit ###
* At the top right corner, Click Either "Save for Later" if not complete or "Summary and Submit" to review and submit.
* Click "Submit Expense Report" button at bottom of report.

###  Overview ###
![picture](overview_screenshot.png)
